hope that i dont fall in love with you
cause falling in love just makes me blue
well the music plays and you display your heart for me to see
i had a beer and now i hear you calling out for me
and i hope that i dont fall in love with you

well the room is crowded people everywhere
and i wonder should i offer you a chair
well if you sit down with this old clown take that frown and break it
before the evenings gone away i think that we could make it
and i hope that i dont fall in love with you

well the night does funny things inside a man
these old tomcat feelings you dont understand
well i turn around to look at you you light a cigarette
i wish i had the guts to bum one but weve never met
and i hope that i dont fall in love with you

i can see that you are lonesome just like me and it being late
youd like some some company
well i turn around to look at you and you look back at me
the guy youre with has up and split the chair next to yous free
and i hope that you dont fall in love with me

now its closing time the musics fading out
last call for drinks ill have another stout
well i turn around to look at you youre nowhere to be found
i search the place for your lost face guess ill have another round
and i think that i just fell in love with you

a good man is hard to find
only strangers sleep in my bed
my favorite words are goodbye
and my favorite color is red

i always play russian roulette in my head
its seventeen black and twentynine red
how far from the gutter how far from the pew
ill always remember to forget about you

a good man is hard to find
only strangers sleep in my bed
my favorite words are goodbye
and my favorite color is red

a long dead soldier looks out from the frame
no one remembers his war no one
remembers his name
go out to the meadow scare off all the crows
it does nothing but rain here and nothing 
will grow

a good man is hard to find
only strangers sleep in my bed
my favorite words are goodbye
and my favorite color is red

well i hung on to marys stump
i danced with a soldiers glee
with a rum soaked crook
and a big fat laugh
i spent my last dollar on thee
i saw bill bones gave him a yell
kehoe spiked the nog
with a chain link fence
and a scrap iron jaw
cookin up a filipino box spring hog
spider rolled in from
hollister burn
with a oneeyed stolen mare
donned himself with chicken fat
sawin on a jaw bone violin there
kathleen was sittin down
in little reds recovery room
in her criminal underwear bra
i was naked to the waist
with my fierce black hound
and im cookin up a filipino box spring hog
cookin up a filipino box spring hog
cookin up a filipino box spring hog

dig a big pit in a dirt alley road
fill it with madrone and bay
stinks like hell
and the neighbors complain
dont give a hoot what they say
slap that hog
gotta roll em over twice
baste him with a sweeping broom
you gotta swat them flies
and chain up the dogs
cookin up a filipino box spring hog
cookin up a filipino box spring hog

rattle snake piccata with grapes and figs
old brown betty with a yellow wig
taint the mince meat filagree
and it aint the turkey neck stew
and it aint them bruleed
okra seeds though she
made them especially for you
worse won a prize for her
bottom black pie
the beans got to thrown to the dogs
jaheseus christ i can always
make room when theyre
cookin up a filipino box spring hog
cookin up a filipino box spring hog
cookin up a filipino box spring hog

well its 9th and hennepin
and all the donuts have
names that sound like prostitutes
and the moons teeth marks are
on the sky like a tarp thrown over all this
and the broken umbrellas like
dead birds and the steam
comes out of the grill like
the whole goddamned town is ready to blow
and the bricks are all scarred with jailhouse tattoos
and everyone is behaving like dogs
and the horses are coming down violin road
and dutch is dead on his feet
and the rooms all smell like diesel
and you take on the
dreams of the ones who have slept here
and im lost in the window
i hide on the stairway
i hang in the curtain
i sleep in your hat
and no one brings anything
small into a bar around here
they all started out with bad directions
and the girls behind the counter has a tattooed tear
one for every year hes away she said
such a crumbling beauty but theres
well nothing wrong with her that
$100 wont fix
she has that razor sadness
that only gets worse
with the clang and the thunder
of the southern pacific going by
as the clock ticks out like a dripping faucet
till youre full of rag water and bitters and blue ruin
and you spill out
over the side to anyone wholl listen
and ive seen it
all through the yellow windows
of the evening train
